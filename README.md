## Instructions to view the presentation

Please download the project repository, complete with all of its contents, and open the `index.html` file with your internet browser.
Alternatively, there is a PDF version available, although slides with animations are not properly displayed.
